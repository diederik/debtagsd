"""
WSGI config for debtagsd project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.9/howto/deployment/wsgi/
"""

import os
import sys

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "debtagsd.settings")
os.umask(0o05)

project_root = '/srv/debtags.debian.org/debtagsd'
paths = [project_root]
for path in paths:
    if path not in sys.path:
        sys.path.append(path)

from django.core.wsgi import get_wsgi_application  # noqa: E402
application = get_wsgi_application()
