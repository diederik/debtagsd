from __future__ import annotations
from django.test import TestCase, override_settings
from django.utils import timezone
from backend.unittest import BackendDataMixin, run_housekeeping
from unittest import mock
import datetime


@override_settings(BACKEND_DATADIR="testdata")
class TestHousekeeping(BackendDataMixin, TestCase):
    def test_debiancontributors(self):
        self.users.create("test1", username="test1")

        import debiancontributors  # noqa
        with mock.patch('django.utils.timezone.now', return_value=datetime.datetime(2020, 1, 1, tzinfo=timezone.utc)):
            self.audit_logs.create("start", user=self.user, text="")
        with mock.patch('django.utils.timezone.now', return_value=datetime.datetime(2020, 6, 15, tzinfo=timezone.utc)):
            self.audit_logs.create("end", user=self.user, text="")

        with override_settings(DC_AUTH_TOKEN="test"):
            with mock.patch("debiancontributors.Submission.post", return_value=(True, None), autospec=True) as p:
                run_housekeeping(run_filter=lambda name: name == "exports:backend.DebianContributors")

        self.assertTrue(p.called)
        self.assertEqual(p.call_count, 1)
        submission = p.call_args[0][0]
        self.assertEquals(submission.name, "debtags.debian.org")
        self.assertEquals(len(submission.entries), 1)
        ident, contrib = next(iter(submission.entries.items()))
        self.assertEqual(ident.type, "email")
        self.assertEqual(ident.id, "test")
        self.assertEqual(len(contrib), 1)
        t, c = next(iter(contrib.items()))
        self.assertEqual(t, "tagger")
        self.assertEqual(c.type, "tagger")
        self.assertEqual(c.begin, datetime.date(2020, 1, 1))
        self.assertEqual(c.end, datetime.date(2020, 6, 15))
