/**
 * jQuery Cookie plugin
 *
 * Copyright (c) 2010 Klaus Hartl (stilbuero.de)
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 */
(function( $ ){
    $.cookie = function (key, value, options) {

        // key and at least value given, set cookie...
        if (arguments.length > 1 && String(value) !== "[object Object]") {
            options = jQuery.extend({}, options);

            if (value === null || value === undefined) {
                options.expires = -1;
            }

            if (typeof options.expires === 'number') {
                var days = options.expires, t = options.expires = new Date();
                t.setDate(t.getDate() + days);
            }

            value = String(value);

            return (document.cookie = [
                encodeURIComponent(key), '=',
                options.raw ? value : encodeURIComponent(value),
                options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
                options.path ? '; path=' + options.path : '',
                options.domain ? '; domain=' + options.domain : '',
                options.secure ? '; secure' : ''
            ].join(''));
        }

        // key and possibly options given, get cookie...
        options = value || {};
        var result, decode = options.raw ? function (s) { return s; } : decodeURIComponent;
        return (result = new RegExp('(?:^|; )' + encodeURIComponent(key) + '=([^;]*)').exec(document.cookie)) ? decode(result[1]) : null;
    };
})( jQuery );


// End of JQuery cookie plugin, beginning of debtags-ui code

(function($) {
"use strict";

// Persistent settings saved in a cookie
class Settings
{
    constructor(options)
    {
        this.options = $.extend({
            // Cookie where we store the settings
            cookie_name: "settings",
            // Number of days these settings are kept
            expires: 30,
        }, options);
        this._defaults = {};
        this._value = null;
        this._load();
    }

    _load()
    {
        let str = $.cookie(this.options.cookie_name);
        if (!str)
            this._value = {};
        else
            this._value = $.parseJSON(str);
    }

    // Save the settings
    save()
    {
        var str = JSON.stringify(this._value);
        $.cookie(this.options.cookie_name, str, { expires: this.options.expires });
    }

    /*
     * Set some default settings
     *
     * The contents of the given object ({}) are merged into the existing
     * defaults.
     *
     * Defaults are not saved in the settings cookie.
     */
    defaults(vals)
    {
        // Merge these defaults into ours
        $.extend(this._defaults, vals);
    }

    // Get or set a value.
    //
    // This function does not automatically save the settings: save() needs
    // to be called to make changes persistant.
    value(key, val)
    {
        if (arguments.length == 1)
        {
            if (key in this._value)
                return this._value[key];
            else
            {
                var res = this._defaults[key];
                if (res instanceof Array)
                    res = $.extend(true, [], res)
                else if (res instanceof Object)
                    res = $.extend(true, {}, res)
                return this._value[key] = res;
            }
        }
        else
            return this._value[key] = val;
    }
}

let global_settings = null;

/*
 * Return the global singleton settings instance.
 *
 * If it does not exist yet, then it is created with the given options.
 *
 * This allows the settings of a session to be customized by calling
 * $.settings as the first thing in the page onload handler.
 */
function get_settings(options)
{
    if (global_settings == null)
        global_settings = new Settings(options);
    return global_settings;
};


/**
 * Group of distributions.
 *
 * The element is the <li> element that contains the distribution name
 */
class HideDistGroup
{
    constructor(element, editor)
    {
        this.element = $(element);
        this.editor = editor;
        this.items = [];
        this.element.find("ul > li > input[type=checkbox]").each((idx, el) => {
            this.items.push($(el));
        });
        this.grouper = this.element.find("> input[type=checkbox]");
        this.name = this.grouper.attr("id").substr(13);
        this.element.on("click", "input[type=checkbox]", evt => {
            const name = evt.target.id.substr(13);
            if (!name.includes("-"))
            {
                const val = evt.target.checked;
                for (let item of this.items)
                    $(item).prop("checked", val);
            }
            this.update_grouper();
        });
    }

    update_grouper()
    {
        let all_true = true;
        let all_false = true;
        for (let item of this.items)
        {
            if (item.is(":checked"))
                all_false = false;
            else
                all_true = false;
        }
        if (all_true)
            this.grouper.prop("checked", true);
        else if (all_false)
        {
            this.grouper.prop("checked", false);
        }
        this.grouper[0].indeterminate = !(all_true || all_false);
    }

    set(hide_dists)
    {
        for (let item of this.items)
        {
            const el_name = item.attr("id").substr(13);
            item.attr("checked", hide_dists[el_name]);
        }
        this.update_grouper();
    }

    get(hide_dists)
    {
        for (let item of this.items)
        {
            const el_name = item.attr("id").substr(13);
            hide_dists[el_name] = item.is(":checked");
        }
    }
}

class SettingsEditor
{
    constructor(element)
    {
        this.element = $(element);

        // Get the singleton settings instance
        this._settings = window.debtags.settings();

        // Default settings for what we manage
        this._settings.defaults({
            // Distributions the user is not interested in
            hide_dists: {},
        });

        // Instantiate the check groups
        this.hide_dist_groups = [];
        this.element.find("#hide-dists > li").each((idx, el) => {
            this.hide_dist_groups.push(new HideDistGroup(el, this));
        });

        let hide_dists = this._settings.value("hide_dists");
        for (let group of this.hide_dist_groups)
            group.set(hide_dists);

        this.element.find("#dui-settings-save").click(evt => {
            for (let group of this.hide_dist_groups)
                group.get(hide_dists);
            this._settings.save();
            this.element.modal("hide");
            this.element.trigger("settings_save");
        });
    }

    open()
    {
        this.element.modal();
    }
}

window.debtags = $.extend(window.debtags || {}, {
    settings: get_settings,
    SettingsEditor: SettingsEditor,
});

})(jQuery);
