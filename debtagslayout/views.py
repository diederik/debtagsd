from __future__ import annotations
import urllib.parse
from django.views.generic.base import TemplateView
from debtagslayout.mixins import DebtagsLayoutMixin
import logging

log = logging.getLogger(__name__)


class DebtagsTemplateView(DebtagsLayoutMixin, TemplateView):
    pass


class Error403(DebtagsLayoutMixin, TemplateView):
    def dispatch(self, *args, **kw):
        try:
            res = super().dispatch(*args, **kw)
            res.status_code = 403
            return res
        except Exception as e:
            log.error("403 view failed: %s", e, exc_info=e)
            raise

    def get_template_names(self):
        return [self.kwargs.get("template_name", "403.html")]


class Error404(DebtagsLayoutMixin, TemplateView):
    def dispatch(self, *args, **kw):
        try:
            res = super().dispatch(*args, **kw)
            res.status_code = 404
            return res
        except Exception as e:
            log.error("404 view failed: %s", e, exc_info=e)
            raise

    def get_template_names(self):
        return [self.kwargs.get("template_name", "404.html")]

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)

        exception_repr = self.kwargs["exception"].__class__.__name__
        # Try to get an "interesting" exception message, if any (and not the ugly
        # Resolver404 dictionary)
        try:
            message = self.kwargs["exception"].args[0]
        except (AttributeError, IndexError):
            pass
        else:
            if isinstance(message, str):
                exception_repr = message
        ctx['request_path'] = urllib.parse.quote(self.request.path)
        ctx['exception'] = exception_repr
        return ctx


class Error500(DebtagsLayoutMixin, TemplateView):
    def dispatch(self, *args, **kw):
        try:
            res = super().dispatch(*args, **kw)
            res.status_code = 500
            return res
        except Exception as e:
            log.error("500 view failed: %s", e, exc_info=e)
            raise

    def get_template_names(self):
        return [self.kwargs.get("template_name", "500.html")]
