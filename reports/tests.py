from __future__ import annotations
from django.test import TestCase, override_settings
from django.urls import reverse
from backend.unittest import BackendDataMixin
import backend.models as bmodels
import reports.models as rmodels


@override_settings(BACKEND_DATADIR="testdata")
class TestModel(BackendDataMixin, TestCase):
    def test_simple_queries(self):
        tagdb = bmodels.tag_debtags_db()
        series_human, series_robot, outliers = rmodels.chart_data_tag_count(tagdb)
        self.assertEquals(len(series_human), 13)
        self.assertEquals(series_robot, [(i, 0) for i in range(13)])
        self.assertEquals(outliers, {})


@override_settings(BACKEND_DATADIR="testdata")
class TestView(BackendDataMixin, TestCase):
    def test_simple_queries(self):
        response = self.client.get(reverse("report_index"))
        self.failUnlessEqual(response.status_code, 200)
        # self.assertEquals(response.content, "")

        response = self.client.get(reverse("report_stats"))
        self.failUnlessEqual(response.status_code, 200)
        # self.assertEquals(response.content, "")

        response = self.client.get(reverse("report_getting_started"))
        self.failUnlessEqual(response.status_code, 200)
        # self.assertEquals(response.content, "")

        response = self.client.get(reverse("report_todo"))
        self.failUnlessEqual(response.status_code, 200)
        # self.assertEquals(response.content, "")

        response = self.client.get(reverse("report_maint_index"))
        self.failUnlessEqual(response.status_code, 200)
        # self.assertEquals(response.content, "")

        response = self.client.get(reverse("report_todo_maint", kwargs=dict(email="enrico@debian.org")))
        self.failUnlessEqual(response.status_code, 200)
        # self.assertEquals(response.content, "")

        response = self.client.get(reverse("report_checks_list"))
        self.failUnlessEqual(response.status_code, 200)
        # self.assertEquals(response.content, "")

        response = self.client.get(reverse("report_checks_show", kwargs=dict(id="1")))
        self.failUnlessEqual(response.status_code, 200)
        # self.assertEquals(response.content, "")

        response = self.client.get(reverse("report_pkginfo", kwargs=dict(name="debtags")))
        self.failUnlessEqual(response.status_code, 200)
        # self.assertEquals(response.content, "")

        response = self.client.get(reverse("report_facets"))
        self.failUnlessEqual(response.status_code, 200)
        # self.assertEquals(response.content, "")

        response = self.client.get(reverse("report_facet", kwargs=dict(name="role")))
        self.failUnlessEqual(response.status_code, 200)
        # self.assertEquals(response.content, "")

        response = self.client.get(reverse("report_tag", kwargs=dict(name="role::program")))
        self.failUnlessEqual(response.status_code, 200)
        # self.assertEquals(response.content, "")

        response = self.client.get(reverse("report_apriori_rules"))
        self.failUnlessEqual(response.status_code, 200)
        # self.assertEquals(response.content, "")
