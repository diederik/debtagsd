Facet: admin
Status: needing-review
Description: System Administration
 Which system administration activities the package may perform

Tag: admin::accounting
Description: Accounting

Tag: admin::automation
Description: Automation and Scheduling
 Automating the execution of software in the system.

Tag: admin::backup
Description: Backup and Restoration

Tag: admin::benchmarking
Description: Benchmarking

Tag: admin::boot
Description: System Boot

Tag: admin::cluster
Description: Clustering

Tag: admin::configuring
Description: Configuration Tool

Tag: admin::file-distribution
Description: File Distribution

Tag: admin::filesystem
Description: Filesystem Tool
 Creation, maintenance, and use of filesystems

Tag: admin::forensics
Description: Forensics and Recovery
 Recovering lost or damaged data.
 This tag will be split into admin::recovery
 and security::forensics.

Tag: admin::hardware
Description: Hardware Support

Tag: admin::install
Description: System Installation

Tag: admin::issuetracker
Description: Issue Tracker

Tag: admin::kernel
Description: Kernel or Modules

Tag: admin::logging
Description: Logging

Tag: admin::login
Description: Login
 Logging into the system

Tag: admin::monitoring
Description: Monitoring

Tag: admin::package-management
Description: Package Management

Tag: admin::power-management
Description: Power Management

Tag: admin::recovery
Description: Data Recovery

Tag: admin::user-management
Description: User Management

Tag: admin::virtualization
Description: Virtualization
 This is not hardware emulation, but rather those facilities that allow to
 create many isolated compartments inside the same system.

Tag: admin::TODO
Description: Need an extra tag
 The package can be categorised along this facet, but the right tag for it is
 missing.
 .
 Mark a package with this tag to signal the vocabulary maintainers of cases
 where the current tag set is lacking.


Facet: culture
Status: complete
Description: Culture
 The culture for which the package provides special support

Tag: culture::afrikaans
Description: Afrikaans

Tag: culture::arabic
Description: Arabic

Tag: culture::basque
Description: Basque

Tag: culture::bengali
Description: Bengali

Tag: culture::bokmaal
Description: Norwegian Bokmaal

Tag: culture::bosnian
Description: Bosnian

Tag: culture::brazilian
Responsible: stratus@acm.org
Description: Brazilian

Tag: culture::british
Description: British

Tag: culture::bulgarian
Description: Bulgarian

Tag: culture::catalan
Description: Catalan

Tag: culture::chinese
Description: Chinese

Tag: culture::czech
Description: Czech

Tag: culture::croatian
Description: Croatian

Tag: culture::danish
Description: Danish

Tag: culture::dutch
Description: Dutch

Tag: culture::esperanto
Description: Esperanto

Tag: culture::estonian
Description: Estonian

Tag: culture::faroese
Description: Faroese

Tag: culture::farsi
Description: Farsi

Tag: culture::finnish
Description: Finnish

Tag: culture::french
Description: French

Tag: culture::galician
Description: Galician

Tag: culture::german
Description: German

Tag: culture::greek
Description: Greek

Tag: culture::hebrew
Description: Hebrew

Tag: culture::hindi
Description: Hindi

Tag: culture::hungarian
Description: Hungarian

Tag: culture::icelandic
Description: Icelandic

Tag: culture::indonesian
Description: Indonesian

Tag: culture::irish
Description: Irish (Gaeilge)

Tag: culture::italian
Description: Italian

Tag: culture::japanese
Description: Japanese

Tag: culture::korean
Description: Korean

Tag: culture::latvian
Description: Latvian

Tag: culture::lithuanian
Description: Lithuanian

Tag: culture::mongolian
Description: Mongolian

Tag: culture::nynorsk
Description: Norwegian Nynorsk

Tag: culture::norwegian
Description: Norwegian

Tag: culture::polish
Description: Polish

Tag: culture::portuguese
Description: Portuguese

Tag: culture::punjabi
Description: Punjabi

Tag: culture::romanian
Description: Romanian

Tag: culture::russian
Description: Russian

Tag: culture::serbian
Description: Serbian

Tag: culture::slovak
Description: Slovak

Tag: culture::spanish
Description: Spanish

Tag: culture::swedish
Description: Swedish

Tag: culture::taiwanese
Description: Taiwanese

Tag: culture::tajik
Description: Tajik

Tag: culture::tamil
Description: Tamil

Tag: culture::thai
Description: Thai

Tag: culture::turkish
Description: Turkish

Tag: culture::ukrainian
Description: Ukrainian

Tag: culture::uzbek
Description: Uzbek

Tag: culture::welsh
Description: Welsh

Tag: culture::TODO
Description: Need an extra tag
 The package can be categorised along this facet, but the right tag for it is
 missing.
 .
 Mark a package with this tag to signal the vocabulary maintainers of cases
 where the current tag set is lacking.


Facet: devel
Status: needing-review
Description: Software Development
 How the package is related to the field of software development

Tag: devel::bugtracker
Description: Bug Tracking

Tag: devel::buildtools
Description: Build Tool

Tag: devel::code-generator
Description: Code Generation
 Parser, lexer and other code generators

Tag: devel::compiler
Description: Compiler

Tag: devel::debian
Description: Debian
 Tools, documentation, etc. of use primarily to Debian developers.

Tag: devel::debugger
Description: Debugging

Tag: devel::doc
Description: Documentation

Tag: devel::docsystem
Description: Literate Programming
 Tools and auto-documenters

Tag: devel::ecma-cli
Description: ECMA CLI
 Tools and libraries for development with implementations of
 the ECMA CLI (Common Language Infrastructure), like Mono
 or DotGNU Portable.NET.

Tag: devel::editor
Description: Source Editor

Tag: devel::examples
Description: Examples

Tag: devel::ide
Description: IDE
 Integrated Development Environment

Tag: devel::interpreter
Description: Interpreter

Tag: devel::i18n
Description: Internationalization

Tag: devel::lang:ada
Description: Ada Development

Tag: devel::lang:c
Description: C Development

Tag: devel::lang:c++
Description: C++ Development

Tag: devel::lang:c-sharp
Description: C# Development

Tag: devel::lang:fortran
Description: Fortran Development

Tag: devel::lang:haskell
Description: Haskell Development

Tag: devel::lang:java
Description: Java Development

Tag: devel::lang:ecmascript
Description: Ecmascript/JavaScript Development

Tag: devel::lang:lisp
Description: Lisp Development

Tag: devel::lang:lua
Description: Lua Development

Tag: devel::lang:ml
Description: ML Development

Tag: devel::lang:objc
Description: Objective-C Development

Tag: devel::lang:ocaml
Responsible: zack@debian.org
Description: OCaml Development

Tag: devel::lang:octave
Description: GNU Octave Development

Tag: devel::lang:pascal
Description: Pascal Development

Tag: devel::lang:perl
Description: Perl Development

Tag: devel::lang:posix-shell
Description: POSIX shell

Tag: devel::lang:php
Description: PHP Development

Tag: devel::lang:pike
Description: Pike Development

Tag: devel::lang:prolog
Description: Prolog Development

Tag: devel::lang:python
Description: Python Development

Tag: devel::lang:r
Description: GNU R Development

Tag: devel::lang:ruby
Description: Ruby Development

Tag: devel::lang:scheme
Description: Scheme Development

Tag: devel::lang:sql
Description: SQL

Tag: devel::lang:tcl
Description: Tcl Development

Tag: devel::library
Description: Libraries

Tag: devel::machinecode
Description: Machine Code
 Assemblers and other machine-code development tools.

Tag: devel::modelling
Description: Modelling
 Programs and libraries that support creation of software models
 with modelling languages like UML or OCL.

Tag: devel::packaging
Description: Packaging
 Tools for packaging software.

Tag: devel::prettyprint
Description: Prettyprint
 Code pretty-printing and indentation/reformatting.

Tag: devel::profiler
Description: Profiling
 Profiling and optimization tools.

Tag: devel::rcs
Description: Revision Control
 RCS (Revision Control System) and SCM (Software Configuration Manager)

Tag: devel::rpc
Description: RPC
 Remote Procedure Call, Network transparent programming

Tag: devel::runtime
Description: Runtime Support
 Runtime environments of various languages and systems.

Tag: devel::testing-qa
Description: Testing and QA
 Tools for software testing and quality assurance.

Tag: devel::ui-builder
Description: User Interface
 Tools for designing user interfaces.

Tag: devel::web
Description: Web
 Web-centric frameworks, CGI libraries and other web-specific development
 tools.

Tag: devel::TODO
Description: Need an extra tag
 The package can be categorised along this facet, but the right tag for it is
 missing.
 .
 Mark a package with this tag to signal the vocabulary maintainers of cases
 where the current tag set is lacking.


Facet: made-of
Status: needing-review
Description: Made Of
 The languages or data formats used to make the package

Tag: made-of::audio
Description: Audio

Tag: made-of::dictionary
Description: Dictionary

Tag: made-of::font
Description: Font

Tag: made-of::html
Description: HTML, Hypertext Markup Language

Tag: made-of::icons
Description: Icons

Tag: made-of::info
Description: Documentation in Info Format

Tag: made-of::man
Description: Manuals in Nroff Format

Tag: made-of::pdf
Description: PDF Documents

Tag: made-of::postscript
Description: PostScript

Tag: made-of::sgml
Description: SGML, Standard Generalized Markup Language

Tag: made-of::svg
Description: SVG, Scalable Vector Graphics

Tag: made-of::tex
Description: TeX, LaTeX and DVI

Tag: made-of::vrml
Description: VRML, Virtual Reality Markup Language

Tag: made-of::xml
Description: XML

Tag: made-of::TODO
Description: Need an extra tag
 The package can be categorised along this facet, but the right tag for it is
 missing.
 .
 Mark a package with this tag to signal the vocabulary maintainers of cases
 where the current tag set is lacking.


Facet: interface
Status: needing-review
Description: User Interface
 What kind of user interface the package provides

Tag: interface::3d
Description: Three-Dimensional

Tag: interface::commandline
Description: Command Line

Tag: interface::daemon
Description: Daemon
 Runs in background, only a control interface is provided, usually on
 commandline.

Tag: interface::framebuffer
Description: Framebuffer

Tag: interface::shell
Description: Command Shell

Tag: interface::svga
Description: Console SVGA

Tag: interface::text-mode
Description: Text-based Interactive

Tag: interface::web
Description: World Wide Web

Tag: interface::x11
Description: X Window System

Tag: interface::TODO
Description: Need an extra tag
 The package can be categorised along this facet, but the right tag for it is
 missing.
 .
 Mark a package with this tag to signal the vocabulary maintainers of cases
 where the current tag set is lacking.


Facet: implemented-in
Description: Implemented in
 What language the software is implemented in

Tag: implemented-in::ada
Description: Ada

Tag: implemented-in::c
Description: C

Tag: implemented-in::c++
Description: C++

Tag: implemented-in::c-sharp
Description: C#

Tag: implemented-in::fortran
Description: Fortran

Tag: implemented-in::haskell
Description: Haskell

Tag: implemented-in::java
Description: Java

Tag: implemented-in::ecmascript
Description: Ecmascript/Javascript

Tag: implemented-in::lisp
Description: Lisp

Tag: implemented-in::lua
Description: Lua

Tag: implemented-in::ml
Description: ML

Tag: implemented-in::objc
Description: Objective C

Tag: implemented-in::ocaml
Responsible: zack@debian.org
Description: OCaml

Tag: implemented-in::perl
Description: Perl

Tag: implemented-in::php
Description: PHP

Tag: implemented-in::pike
Description: Pike

Tag: implemented-in::python
Description: Python

Tag: implemented-in::r
Description: GNU R

Tag: implemented-in::ruby
Description: Ruby

Tag: implemented-in::scheme
Description: Scheme

Tag: implemented-in::shell
Description: sh, bash, ksh, tcsh and other shells

Tag: implemented-in::tcl
Description: Tcl, Tool Command Language

Tag: implemented-in::TODO
Description: Need an extra tag
 The package can be categorised along this facet, but the right tag for it is
 missing.
 .
 Mark a package with this tag to signal the vocabulary maintainers of cases
 where the current tag set is lacking.


Facet: works-with
Status: needing-review
Description: Works with
 What kind of data (or even processes, or people) the package can work with

Tag: works-with::3dmodel
Implies: works-with
Description: 3D Model

Tag: works-with::archive
Implies: works-with
Description: Archive

Tag: works-with::audio
Description: Audio

Tag: works-with::biological-sequence
Description: Biological Sequence

Tag: works-with::bugs
Description: Bugs or Issues

Tag: works-with::db
Description: Databases

Tag: works-with::dictionary
Description: Dictionaries

Tag: works-with::dtp
Description: Desktop Publishing (DTP)

Tag: works-with::fax
Description: Faxes

Tag: works-with::file
Description: Files

Tag: works-with::font
Description: Fonts

Tag: works-with::graphs
Description: Trees and Graphs

Tag: works-with::im
Description: Instant Messages
 The package can connect to some IM network (or networks).

Tag: works-with::logfile
Description: System Logs

Tag: works-with::mail
Description: Email

Tag: works-with::music-notation
Description: Music Notation

Tag: works-with::network-traffic
Description: Network Traffic
 Routers, shapers, sniffers, firewalls and other tools
 that work with a stream of network packets.

Tag: works-with::people
Description: People

Tag: works-with::pim
Description: Personal Information

Tag: works-with::image
Description: Image

Tag: works-with::image:raster
Description: Raster Image
 Images made of dots, such as photos and scans

Tag: works-with::image:vector
Description: Vector Image
 Images made of lines, such as graphs or most clipart

Tag: works-with::software:package
Description: Packaged Software

Tag: works-with::software:running
Description: Running Programs

Tag: works-with::software:source
Description: Source Code

Tag: works-with::spreadsheet
Description: Spreadsheet

Tag: works-with::text
Description: Text

Tag: works-with::unicode
Description: Unicode
 Please do not tag programs with simple unicode support,
 doing so would make this tag useless.
 Ultimately all applications should have unicode support.

Tag: works-with::vcs
Description: Version control system

Tag: works-with::video
Description: Video and Animation

Tag: works-with::TODO
Description: Need an extra tag
 The package can be categorised along this facet, but the right tag for it is
 missing.
 .
 Mark a package with this tag to signal the vocabulary maintainers of cases
 where the current tag set is lacking.


Facet: scope
Description: Scope
 Characterization by scale of coverage 

Tag: scope::utility
Description: Utility
 A narrow-scoped program for particular use case or few use cases. It
 only does something 10-20% of users in the field will need. Often has
 functionality missing from related applications.

Tag: scope::application
Description: Application
 Broad-scoped program for general use. It probably has functionality
 for 80-90% of use cases. The pieces that remain are usually to be
 found as utilities.

Tag: scope::suite
Description: Suite
 Comprehensive suite of applications and utilities on the scale of
 desktop environment or base operating system.

Tag: scope::TODO
Description: Need an extra tag
 The package can be categorised along this facet, but the right tag for it is
 missing.
 .
 Mark a package with this tag to signal the vocabulary maintainers of cases
 where the current tag set is lacking.


Facet: special
Status: draft
Description: Service tags
 Group of special tags

Tag: special::unreviewed
Implies: special
Description: !Not yet tagged packages!

Tag: special::TODO
Description: Need an extra tag
 The package can be categorised along this facet, but the right tag for it is
 missing.
 .
 Mark a package with this tag to signal the vocabulary maintainers of cases
 where the current tag set is lacking.


Facet: role
Status: draft
Description: Role
 Role performed by the package

Tag: role::app-data
Description: Application Data

Tag: role::data
Description: Standalone Data

Tag: role::debug-symbols
Description: Debugging symbols
 Debugging symbols.

Tag: role::devel-lib
Description: Development Library
 Library and header files used in software development or building.

Tag: role::documentation
Description: Documentation

Tag: role::dummy
Description: Dummy Package
 Packages used for upgrades and transitions.

Tag: role::examples
Description: Examples

Tag: role::kernel
Description: Kernel and Modules
 Packages that contain only operating system kernels and kernel modules.

Tag: role::metapackage
Description: Metapackage
 Packages that install suites of other packages.

Tag: role::plugin
Description: Plugin
 Add-on, pluggable program fragments enhancing functionality
 of some program or system.

Tag: role::program
Description: Program
 Executable computer program.

Tag: role::shared-lib
Description: Shared Library
 Shared libraries used by one or more programs.

Tag: role::source
Description: Source Code
 Human-readable code of a program, library or a part thereof.

Tag: role::TODO
Description: Need an extra tag
 The package can be categorised along this facet, but the right tag for it is
 missing.
 .
 Mark a package with this tag to signal the vocabulary maintainers of cases
 where the current tag set is lacking.


Facet: suite
Status: controversial
Comment: What is considered a 'suite'?  So far the idea was to just use tags in this facet to group together related packages.
Description: Application Suite
 Groups together related packages

Tag: suite::apache
Description: Apache

Tag: suite::bsd
Description: BSD
 Berkeley Software Distribution, sometimes called Berkeley Unix or BSD Unix,
 and its family of descendants: FreeBSD, NetBSD or OpenBSD.
 .
 Link: http://en.wikipedia.org/wiki/Berkeley_Software_Distribution

Tag: suite::debian
Description: Debian
 Packages specific to Debian - look into "Software Development::Debian" for 
 Debian Development.

Tag: suite::eclipse
Description: Eclipse
 Eclipse tool platform and plugins.

Tag: suite::emacs
Description: Emacs

Tag: suite::gforge
Description: GForge
 A collaborative development platform.

Tag: suite::gimp
Description: The GIMP

Tag: suite::gkrellm
Description: GKrellM Monitors

Tag: suite::gnome
Description: GNOME

Tag: suite::gnu
Description: GNU
 Gnu's Not Unix. The package is part of the official GNU project

Tag: suite::gnustep
Description: GNUstep
 GNUstep Desktop and WindowMaker

Tag: suite::gpe
Description: GPE
 GPE Palmtop Environment

Tag: suite::kde
Description: KDE

Tag: suite::mozilla
Description: Mozilla
 Mozilla Browser and extensions

Tag: suite::netscape
Description: Netscape Navigator
 The pre-6.0 versions of netscape browser

Tag: suite::openoffice
Description: OpenOffice.org

Tag: suite::opie
Description: Open Palmtop (OPIE)

Tag: suite::roxen
Description: Roxen

Tag: suite::samba
Description: Samba

Tag: suite::webmin
Description: Webmin

Tag: suite::xfce
Description: XFce
 Lightweight desktop environment for X11.

Tag: suite::xmms
Description: XMMS

Tag: suite::xmms2
Description: XMMS 2

Tag: suite::zope
Description: Zope
 The Zope (web) publishing platform.

Tag: suite::TODO
Description: Need an extra tag
 The package can be categorised along this facet, but the right tag for it is
 missing.
 .
 Mark a package with this tag to signal the vocabulary maintainers of cases
 where the current tag set is lacking.


Facet: uitoolkit
Status: needing-review
Description: Interface Toolkit
 Which interface toolkit the package provides

Tag: uitoolkit::athena
Implies: uitoolkit
Description: Athena Widgets

Tag: uitoolkit::fltk
Description: FLTK

Tag: uitoolkit::glut
Description: GLUT

Tag: uitoolkit::gnustep
Description: GNUstep

Tag: uitoolkit::gtk
Implies: uitoolkit
Description: GTK

Tag: uitoolkit::motif
Implies: uitoolkit
Description: Lesstif/Motif

Tag: uitoolkit::ncurses
Implies: uitoolkit
Description: Ncurses TUI

Tag: uitoolkit::qt
Implies: uitoolkit
Description: Qt

Tag: uitoolkit::sdl
Implies: uitoolkit
Description: SDL

Tag: uitoolkit::tk
Implies: uitoolkit
Description: Tk

Tag: uitoolkit::wxwidgets
Implies: uitoolkit
Description: wxWidgets

Tag: uitoolkit::xlib
Implies: uitoolkit
Description: X library

Tag: uitoolkit::TODO
Description: Need an extra tag
 The package can be categorised along this facet, but the right tag for it is
 missing.
 .
 Mark a package with this tag to signal the vocabulary maintainers of cases
 where the current tag set is lacking.


Facet: use
Status: controversial
Description: Purpose
 The general purpose of the software

Tag: use::analysing
Description: Analysing
 Software for turning data into knowledge.

Tag: use::browsing
Description: Browsing

Tag: use::calculating
Description: Calculating

Tag: use::chatting
Description: Chatting

Tag: use::checking
Description: Checking
 All sorts of checking, checking a filesystem for validity, checking
 a document for incorrectly spelled words, checking a network for
 routing problems. Verifying.

Tag: use::comparing
Description: Comparing
 To find what relates or differs in two or more objects.

Tag: use::compressing
Description: Compressing

Tag: use::configuring
Description: Configuration

Tag: use::converting
Implies: file-formats, application
Description: Data Conversion

Tag: use::dialing
Description: Dialup Access

Tag: use::downloading
Description: Downloading

Tag: use::driver
Description: Hardware Driver

Tag: use::editing
Description: Editing

Tag: use::entertaining
Description: Entertaining

Tag: use::filtering
Description: Filtering

Tag: use::gameplaying
Description: Game Playing

Tag: use::learning
Description: Learning

Tag: use::login
Description: Login

Tag: use::measuring
Description: Measuring

Tag: use::monitor
Description: Monitoring

Tag: use::organizing
Description: Data Organisation

Tag: use::playing
Description: Playing Media

Tag: use::printing
Description: Printing

Tag: use::proxying
Description: Proxying

Tag: use::routing
Description: Routing

Tag: use::searching
Description: Searching

Tag: use::scanning
Description: Scanning

Tag: use::simulating
Description: Simulating

Tag: use::storing
Description: Storing

Tag: use::synchronizing
Description: Synchronisation

Tag: use::timekeeping
Description: Time and Clock

Tag: use::transmission
Description: Transmission

Tag: use::typesetting
Description: Typesetting

Tag: use::viewing
Description: Data Visualization

Tag: use::text-formatting
Description: Text Formatting

Tag: use::TODO
Description: Need an extra tag
 The package can be categorised along this facet, but the right tag for it is
 missing.
 .
 Mark a package with this tag to signal the vocabulary maintainers of cases
 where the current tag set is lacking.


Facet: x11
Status: draft
Description: X Window System
 How the package is related to the X Window System

Tag: x11::applet
Description: Applet

Tag: x11::application
Description: Application

Tag: x11::display-manager
Description: Login Manager
 Display managers (graphical login screens)

Tag: x11::font
Description: Font

Tag: x11::library
Description: Library

Tag: x11::screensaver
Description: Screen Saver

Tag: x11::terminal
Description: Terminal Emulator

Tag: x11::theme
Description: Theme

Tag: x11::window-manager
Description: Window Manager

Tag: x11::xserver
Description: X Server and Drivers
  X servers and drivers for the X server (input and video)

Tag: x11::TODO
Description: Need an extra tag
 The package can be categorised along this facet, but the right tag for it is
 missing.
 .
 Mark a package with this tag to signal the vocabulary maintainers of cases
 where the current tag set is lacking.

